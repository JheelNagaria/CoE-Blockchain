
This project is a Proof-of-Concept for Energy Trading in DC Microgrids using Blockchain Technology.

The Blockchain architecture is built on the Ethereum Blockchain Client and a Smart Contract written in Solidity Language has been deployed.

A video demonstrating the entire PoC along with Hardware Prototype can be found on the following Youtube Link:

[Youtube Video Link](https://youtu.be/wsewinEbD4U)

## Team Members

* Awadhut Thube
* Ashish Kamble
* Jheel Nagaria
* Saurabh Gupta

